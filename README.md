# Unity C# hAPI / samples

## Scenes
Parent directory: `Assets/Haply hAPI/Samples/Pantograph/...`

#### Device Setup
Subdirectory: `.../1 - Device Setup`

Visualize device position in real time.

#### Hello Wall
Subdirectory: `.../2 - Hello Wall`

Interact with a solid boundary.